package com.example.transactions.Exceptions;

public class NotFoundException extends RuntimeException{
	public NotFoundException(String message) {
		super(message);
	}

}
