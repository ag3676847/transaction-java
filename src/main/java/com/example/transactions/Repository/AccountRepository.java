package com.example.transactions.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.transactions.Models.Account;

public interface AccountRepository extends JpaRepository<Account,Long> {

}
