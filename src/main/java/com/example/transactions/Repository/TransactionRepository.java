package com.example.transactions.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.transactions.Models.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction,Long> {
	List<Transaction> findByFromAccountIdOrToAccountId(Long fromAccountId, Long toAccountId);
}
