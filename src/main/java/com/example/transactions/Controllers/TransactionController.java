package com.example.transactions.Controllers;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.transactions.Services.TransactionService;
import com.example.transactions.Models.Transaction;

@RestController
@RequestMapping("/api/transactions")
public class TransactionController {
	
	@Autowired
	private TransactionService transactionService;
	
	@PostMapping("/transfer")
	@PreAuthorize("hasRole('User')")
	public ResponseEntity<String> transfer(@RequestParam Long fromAccountId,@RequestParam Long toAccountId,BigDecimal amount){
		transactionService.transfer(fromAccountId, toAccountId, amount);
		return ResponseEntity.ok("Transfer Successful");

	}
	
	@GetMapping("{transactionId}")
	@PreAuthorize("hasRole('User')")
	public Transaction getTransactionHistoryById( @PathVariable("transactionId") Long id ) {
		return transactionService.getTransactionHistoryById(id);
	}
	
	@GetMapping("/history/{accountId}")
    public ResponseEntity<List<Transaction>> getTransactionHistory(@PathVariable Long accountId) {
        List<Transaction> transactionHistory = transactionService.getTransactionHistory(accountId);
        return ResponseEntity.ok(transactionHistory);
    }
}
