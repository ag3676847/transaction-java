package com.example.transactions.Controllers;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.transactions.Models.Account;
import com.example.transactions.Services.AccountService;

@RestController
@RequestMapping("/api/accounts")
public class AccountController {
	@Autowired
	private AccountService accountService;
	

	
	@PostMapping(consumes = "application/json", produces = "application/json")
	@PreAuthorize("hasRole('User')")
	public Account createAccount(@RequestBody Account account) {
		return accountService.save(account);
	}
	
	@PostMapping("/{accountId}/deposit")
	@PreAuthorize("hasRole('User')")
	public ResponseEntity<String> deposit(@PathVariable Long accountId,@RequestParam BigDecimal amount){
		accountService.deposit(accountId, amount);
		return ResponseEntity.ok("Deposit Successful");
	}
	
	@PostMapping("/{accountId}/withdraw")
	@PreAuthorize("hasRole('User')")
	public ResponseEntity<String> withdraw(@PathVariable Long accountId,@RequestParam BigDecimal amount){
		accountService.withDraw(accountId, amount);
		return ResponseEntity.ok("Withdrawal Successful");
	}
	
	@GetMapping("{accountId}")
	@PreAuthorize("hasRole('User')")
	public Account getAccount( @PathVariable("accountId") Long id ) {
		return accountService.getaccount(id);
	}
	
}
