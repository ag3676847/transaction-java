package com.example.transactions.Models;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.example.transactions.Enum.TransactionType;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.*;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Transaction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;
	
	public BigDecimal amount;
	
	private LocalDateTime timestamp;
	
	@Enumerated(EnumType.STRING)
	private TransactionType transactionType;
	
	@ManyToOne
	@JoinColumn (name="from_account_id")
	private Account fromAccount;
	
	@ManyToOne
	@JoinColumn (name="to_account_id")
	private Account toAccount;
}
