package com.example.transactions.Services;

import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.transactions.Exceptions.InsufficientBalanceException;
import com.example.transactions.Exceptions.NotFoundException;
import com.example.transactions.Models.Account;
import com.example.transactions.Repository.AccountRepository;


@Service
public class AccountService {
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private TransactionService transactionService;
	
	public Account save(Account account) {
		validatBalance(account.getBalance(),"Initial Balance should not be negative ");
		return accountRepository.save(account);
	}
	
	public void deposit(Long accountId,BigDecimal amount ) {
		Account account=accountRepository.findById(accountId).orElseThrow(()-> new NotFoundException("Account not found"));
		validateAmount(amount,"Deposit Amount must be greater than Zero");
	    account.setBalance(account.getBalance().add(amount));
		accountRepository.save(account);
		transactionService.deposit(account, amount);
	}
	
	public void withDraw(Long accountId,BigDecimal amount) {
		Account account=accountRepository.findById(accountId).orElseThrow(()-> new NotFoundException("Account not found"));
		validateAmount(amount,"Withdrawl Amount must be greater than Zero");
		BigDecimal newBalance=account.getBalance().subtract(amount);
		if(newBalance.compareTo(BigDecimal.ZERO)<0) {
			throw new InsufficientBalanceException("Insufficient Balance");
		}
		account.setBalance(newBalance);
		accountRepository.save(account);
		transactionService.withdraw(account, amount);
	}
	
	
	
	public Account getaccount(Long id){
		return accountRepository.findById(id).orElseThrow(()-> new NotFoundException("Account not found"));
	}
	
	public void validateAmount(BigDecimal amount,String message) {
		if (amount.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException(message);
		}
	}
	
	public void validatBalance(BigDecimal amount,String message) {
		if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException(message);
		}
	}

}
