package com.example.transactions.Services;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.transactions.Enum.TransactionType;
import com.example.transactions.Exceptions.InsufficientBalanceException;
import com.example.transactions.Exceptions.NotFoundException;
import com.example.transactions.Models.Account;
import com.example.transactions.Repository.AccountRepository;
import com.example.transactions.Repository.TransactionRepository;
import com.example.transactions.Models.Transaction;

@Service
public class TransactionService {
	
	@Autowired
	public TransactionRepository transactionRepository;
	
	@Autowired
	private AccountRepository accountRepository;

	public void deposit(Account account,BigDecimal amount) {
		Transaction transaction=new Transaction();
		transaction.setAmount(amount);
		transaction.setTimestamp(LocalDateTime.now());
		transaction.setTransactionType(TransactionType.DEPOSIT);
		transaction.setFromAccount(account);
		transaction.setToAccount(null);
		transactionRepository.save(transaction);
	}
	
	public void withdraw(Account account,BigDecimal amount) {
		Transaction transaction=new Transaction();
		transaction.setAmount(amount);
		transaction.setTimestamp(LocalDateTime.now());
		transaction.setTransactionType(TransactionType.WITHDRAWL);
		transaction.setFromAccount(account);
		transaction.setToAccount(null);
		transactionRepository.save(transaction);
	}
	
	public void transfer(Long fromAccountId,Long toAccountId,BigDecimal amount) {
		Account fromAccount=accountRepository.findById(fromAccountId).orElseThrow(()-> new NotFoundException("From Account not found"));
		Account toAccount=accountRepository.findById(toAccountId).orElseThrow(()-> new NotFoundException("To Account not found"));
		validateAmount(amount,"Transfer Amount must be greater than Zero");

		BigDecimal newFromBalance=fromAccount.getBalance().subtract(amount);
		if(newFromBalance.compareTo(BigDecimal.ZERO)<0) {
			throw new InsufficientBalanceException("Insufficient Balance For Transfer");
		}
		fromAccount.setBalance(newFromBalance);
		toAccount.setBalance(toAccount.getBalance().add(amount));
		
		accountRepository.save(fromAccount);
		accountRepository.save(toAccount);
		
		Transaction transaction=new Transaction();
		transaction.setAmount(amount);
		transaction.setTimestamp(LocalDateTime.now());
		transaction.setTransactionType(TransactionType.TRANSFER);
		transaction.setFromAccount(fromAccount);
		transaction.setToAccount(toAccount);
		transactionRepository.save(transaction);
	}
	
	
	public Transaction getTransactionHistoryById(long id) {
		return transactionRepository.findById(id).orElseThrow(()-> new NotFoundException("Transaction History not found"));
	}
	
	
	public List<Transaction> getTransactionHistory(Long accountId) {
		 accountRepository.findById(accountId).orElseThrow(()-> new NotFoundException("Account not found"));
		 return transactionRepository.findByFromAccountIdOrToAccountId(accountId, accountId);
	 }
	
	
	public void validateAmount(BigDecimal amount,String message) {
		if (amount.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException(message);
		}
	}
}
