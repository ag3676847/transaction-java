package com.example.transactions.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.User;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.SecurityFilterChain;
import com.example.transactions.Exceptions.UnauthorizedException;
import static org.springframework.security.config.Customizer.withDefaults;



@Configuration
@EnableWebSecurity
public class SecurityConfig {
	
	@Bean
    public AuthenticationEntryPoint authenticationEntryPoint() {
		return new UnauthorizedException();
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.csrf((csrf -> csrf.disable()))
        	
            .authorizeHttpRequests(authorizeRequests ->
                authorizeRequests
                    .requestMatchers("/admin/**").hasRole("ADMIN")
                    .requestMatchers("/api/**").hasRole("USER")
                    .requestMatchers(HttpMethod.POST,"/api/**").hasRole("USER")

                    .anyRequest().authenticated()
            )
            .httpBasic(withDefaults())
           .exceptionHandling((e->e.authenticationEntryPoint(authenticationEntryPoint())));
           
        return http.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    @Bean
    public InMemoryUserDetailsManager userDetailsManager() {
        UserDetails user = User.withUsername("user").password(passwordEncoder().encode("password")).roles("USER").build();
        UserDetails admin = User.withUsername("admin").password(passwordEncoder().encode("adminpassword")).roles("ADMIN").build();
        return new InMemoryUserDetailsManager(user, admin);
    }
}